FROM node:11.6.0-alpine AS builder
COPY . ./battleships-frontend
WORKDIR /battleships-frontend
RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:1.15.8-alpine
COPY --from=builder /battleships-frontend/dist/battleships-frontend/ /usr/share/nginx/html
