export enum GameStatusEnum {
  LOST = "LOST",
  WON = "WON",
  IN_PROGRESS = "IN_PROGRESS"

}
