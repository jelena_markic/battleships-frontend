import {GameStatusResponse} from './game-status-response';
import {Player} from './player';

export class Game {
  status: GameStatusResponse;
  game_id: number;
  opponent_id: number;
  opponentPlayer: Player;
}
