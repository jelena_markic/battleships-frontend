export class GameFullList {
  games: GameDetail[];
}

export class GameDetail {
  gameId: number;
  challengingPlayerId: number;
  challengingPlayerName: string;
  opponentPlayerId: number;
  opponentPlayerName: string;
  challengingPlayerAutopilot: boolean;
  opponentPlayerAutopilot : boolean;
  gameStatus: boolean;

}
