export class GameStatusResponse {
  self: BoardPlayer;
  opponent: BoardPlayer;
  game: GameStatus;
}

export class BoardPlayer {
  board: string[];
  player_id: number;
}

export class GameStatus {
  player_turn: number;
  won: number;
}
