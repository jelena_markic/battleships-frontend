export class ShotsResponse{
  salvo: {};
  game: ShotsGame;
}

export class ShotsGame {
  player_turn: number;
  won: number;
}
