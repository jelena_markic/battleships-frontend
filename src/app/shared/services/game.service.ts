import {Injectable} from "@angular/core";
import {Observable} from "rxjs/index";
import {HttpClient} from "@angular/common/http";
import {endpoints} from "../endpoints";
import {GameList} from '../model/game-list';
import {GameStatusResponse} from '../model/game-status-response';
import {PlayerChallenge} from '../model/player-challenge';
import {SalvoShots} from '../model/salvo-shots';
import {ShotsResponse} from '../model/shots-response';
import {GameFullList} from '../model/game-full-list';

@Injectable()
export class GameService  {

  constructor(
    private httpClient: HttpClient){
  }

  getPlayerGames(playerId: number): Observable<GameList> {
    return this.httpClient.get<GameList>(endpoints().battleships.game_list.replace("{player_id}", playerId.toString()));
  }

  getAllGames(): Observable<GameFullList> {
    return this.httpClient.get<GameFullList>(endpoints().battleships.game_all_list);
  }

  loadGameData(gameId: string, playerId: string) {
    return this.httpClient.get<GameStatusResponse>(endpoints().battleships.game_status.
                replace("{game_id}", gameId).replace("{player_id}", playerId));
  }

  challengePlayer(opponentPlayerId: number, challengingPlayer: number) {
    let challengingPlayerDTO = new PlayerChallenge(challengingPlayer);
    let endpoint = endpoints().battleships.game_challenge.replace("{opponent_id}", opponentPlayerId.toString());
    return this.httpClient.post<GameStatusResponse>(endpoint, challengingPlayerDTO);
  }

  fireShots(playerId: number, gameId: number, salvoShots: SalvoShots) {
    let endpoint = endpoints().battleships.game_shot.replace("{player_id}", playerId.toString()).replace("{game_id}", gameId.toString());
    return this.httpClient.put<ShotsResponse>(endpoint, salvoShots);
  }

  startAutopilot(playerId: number, gameId: number) {
    let endpoint = endpoints().battleships.game_autopilot.replace("{player_id}", playerId.toString()).replace("{game_id}", gameId.toString());
    return this.httpClient.put<any>(endpoint, {});
  }
}
