import {Injectable} from "@angular/core";
import {Observable} from "rxjs/index";
import {HttpClient, HttpParams} from "@angular/common/http";
import {endpoints} from "../endpoints";

@Injectable()
export class BattleshipService  {

  constructor(
    private httpClient: HttpClient){
  }

  // TODO implement battleship service, handling of board
  // game(comb: CombinatorModel): Observable<number> {
  //   let url = endpoints().rlc.combinatorGenerate + '/' + comb.requestedValue
  //     + '/' + comb.minNumGeneratedItems + '/' + comb.maxNumGeneratedItems + '/' + comb.allowedErrorPercentage  + '/' + GameStatusEnum[comb.type];
  //   return this.httpClient.get<number>(url);
  // }

}
