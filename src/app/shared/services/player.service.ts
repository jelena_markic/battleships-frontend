import {Injectable} from "@angular/core";
import {Observable} from "rxjs/index";
import {HttpClient, HttpParams} from "@angular/common/http";
import {endpoints} from "../endpoints";
import {Player} from '../model/player';
import {PlayerList} from '../model/player-list';
import {Game} from '../model/game';

@Injectable()
export class PlayerService  {

  constructor(
    private httpClient: HttpClient){
  }

  getPlayers(): Observable<PlayerList> {
    return this.httpClient.get<PlayerList>(endpoints().battleships.player_list);
  }

  registerPlayer(player: Player) {
    return this.httpClient.post<Player>(endpoints().battleships.player_create, player);
  }

  playerProfile(id: string) {
    return this.httpClient.get<Player>(endpoints().battleships.player_profile.replace("{player_id}", id));
  }

  playerListToMap(players: Player[]): {}{
    return players.reduce(function(map, obj) {
      map[obj.id] = obj.name;
      return map;
    }, {});
  }

}
