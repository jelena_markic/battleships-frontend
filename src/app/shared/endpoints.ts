import {environment} from "../../environments/environment";

export function endpoints() {
  const restApiEndpoint = environment.restApiEndpoint;
  return{
      battleships : {
        player_create: restApiEndpoint + '/player',
        player_profile : restApiEndpoint + '/player/{player_id}',
        player_list : restApiEndpoint + '/player/list',
        game_challenge : restApiEndpoint + '/player/{opponent_id}/game',
        game_status : restApiEndpoint +  '/player/{player_id}/game/{game_id}',
        game_list : restApiEndpoint + '/player/{player_id}/game/list',
        game_all_list : restApiEndpoint + '/player/game/list',
        game_shot : restApiEndpoint + '/player/{player_id}/game/{game_id}',
        game_autopilot : restApiEndpoint + '/player/{player_id}/game/{game_id}/autopilot'
      }
  };
}
