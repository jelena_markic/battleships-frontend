import { CommonModule } from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatProgressBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule, MatGridListModule
} from '@angular/material';
import {FormsModule} from "@angular/forms"
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import {PlayersComponent} from "./components/battleships/players/players.component";
import {PlayerCreateComponent} from "./components/battleships/player-create/player-create.component";
import {FooterComponent} from "./components/footer/footer.component";
import {GameComponent} from './components/battleships/game/game.component';
import {GamesComponent} from './components/battleships/games/games.component';
import {BoardComponent} from './components/battleships/game/board/board.component';
import {AutopilotComponent} from './components/battleships/autopilot/autopilot.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatCardModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatSelectModule,
        MatCardModule,
        MatTabsModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatTableModule,
        FormsModule,
        MatSortModule,
        MatSnackBarModule,
        MatDialogModule,
        MatIconModule,
        MatGridListModule
    ],
  exports: [PlayerCreateComponent],
  declarations: [
      LayoutComponent,
      TopnavComponent,
      SidebarComponent,
      PlayerCreateComponent,
      PlayersComponent,
      AutopilotComponent,
      BoardComponent,
      GameComponent,
      GamesComponent,
      FooterComponent],
  entryComponents: [PlayerCreateComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class LayoutModule {}
