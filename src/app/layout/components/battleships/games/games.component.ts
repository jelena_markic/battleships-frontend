import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Player} from '../../../../shared/model/player';
import {Game} from '../../../../shared/model/game';
import {ActivatedRoute,  Router} from '@angular/router';
import {PlayerService} from '../../../../shared/services/player.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NOT_FOUND} from 'http-status-codes';
import {PlayerList} from '../../../../shared/model/player-list';
import {GameService} from '../../../../shared/services/game.service';
import {MatSelectChange} from '@angular/material/typings/select';
import {GameList} from '../../../../shared/model/game-list';


@Component({
  selector: 'app-combinations',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
  providers: [PlayerService, GameService]
})
export class GamesComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('challengeForm') challengeForm;

  playerService: PlayerService;
  gameService: GameService;

  displayedColumns: string[] = [ 'game_id', 'opponentPlayer.name', 'status', 'play'];
  dataSource = null;

  playerLoaded = false;
  gamesLoaded = false;
  opponentPlayersLoaded = false;

  allPlayers = null;
  opponentPlayers = null;
  playerMap = {};

  currentPlayerId: number;
  opponentPlayerId: number;
  error: boolean;
  games: Game[];

  constructor( private route: ActivatedRoute,
               private router: Router,
               private playerService1: PlayerService,
               private gameService1: GameService,
               private snackBar: MatSnackBar) {
    this.playerService = playerService1;
    this.gameService = gameService1
  }

  ngOnInit() {
    this.loadAllPlayers();
    let playerId = this.route.snapshot.paramMap.get('player_id');
    if (playerId != null){
      this.playerLoaded = false;
      this.loadPlayerData(playerId);
    } else {
      this.playerLoaded = true;
    }
  }

  public getPlayerNameById(id: number): string{
    return this.playerMap[id];
  }

  public loadAllPlayers(){
    this.playerService.getPlayers().subscribe(
        (response: PlayerList) => {
          this.allPlayers = response.players;
          this.playerMap = this.playerService.playerListToMap(this.allPlayers);
          this.loadOpponents(this.currentPlayerId);
        },
        err => this.handleError('Error occurred while retrieving all players data.', err));
  }

  public loadPlayerData(playerId){
    this.playerService.playerProfile(playerId).subscribe(
        (response: Player) => {
            this.currentPlayerId = response.id;
            this.loadPlayerGames(this.currentPlayerId);
            this.loadAllPlayers();
            this.playerLoaded = true;
        },
        err => this.handleError('Error occurred while retrieving player profile.', err));
  }

  public playerSelectionChange(event: MatSelectChange){
    this.opponentPlayersLoaded = false;
    this.currentPlayerId = event.value;
    this.loadPlayerGames(event.value);
    this.loadOpponents(this.currentPlayerId);
  }

  public loadPlayerGames(playerId: number){
    this.gameService.getPlayerGames(playerId).subscribe(
        (response: GameList) => {
          if (response != null){
            this.games = response.games;
            if (this.games != null){
              this.dataSource = new MatTableDataSource(this.games);
              setTimeout(() => {
                this.dataSource.sort = this.sort;
              });
              this.gamesLoaded = true;
              return;
            }
          }
          this.gamesLoaded = false;

        },
        err => this.handleError('Error occurred while retrieving player profile.', err));
  }

  private loadOpponents(currPlayerId) {
    if (this.allPlayers != null){
      this.opponentPlayers = this.allPlayers.filter(function (player) {
        return player.id  != currPlayerId;
      });
      this.opponentPlayersLoaded = true
    }
  }

  onSubmit() {
    this.gameService.challengePlayer(this.opponentPlayerId, this.currentPlayerId).subscribe(
        (response: any) => {},
        err => this.handleError("Error occurred challenging a player.", err),
        () => this.handleSuccess());
  }

  public handleSuccess(){
    this.snackBar.open('Successfully challenged.', 'SUCCESS', { duration: 2000, verticalPosition: 'top', panelClass: 'snackBar-success' });
    this.opponentPlayerId = null;
    this.loadPlayerGames(this.currentPlayerId);
    this.challengeForm.resetForm();
  }


  public handleError(msg: string, error: HttpErrorResponse) {
    let message = 'Error occurred!';
    if (error.status == NOT_FOUND ) {
      message = "Player not found.";
      this.error = true;
    }
    this.snackBar.open(message, 'ERROR', { duration: 2000, verticalPosition: 'top'});
  }

}
