import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {BoardPlayer, GameStatus} from '../../../../../shared/model/game-status-response';
import {MatSnackBar, MatTable, MatTableDataSource} from '@angular/material';
import {GameService} from '../../../../../shared/services/game.service';
import {PlayerList} from '../../../../../shared/model/player-list';
import {SalvoShots} from '../../../../../shared/model/salvo-shots';
import {HttpErrorResponse} from '@angular/common/http';
import {NOT_FOUND} from 'http-status-codes';
import {ShotsResponse} from '../../../../../shared/model/shots-response';
import {environment} from '../../../../../../environments/environment';
import {PusherService} from '../../../../../shared/services/pusher.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardComponent implements OnInit {

  @Input() boardPlayer: BoardPlayer;
  @Input() opponent: boolean;
  @Input() playerTurn: number;
  @Input() playerId: number;
  @Input() gameId: number;
  @Input() gameFinished: boolean;

  gameService: GameService;

  board = new Array(10);
  dataSource = null;

  displayedColumns: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

  shotsCount = 0;
  shotsSet: Set<string>;
  shootingEnabled = false;

  constructor(private gameService1: GameService,
              private snackBar: MatSnackBar) {
    this.gameService = gameService1;
  }

  ngOnChanges() {
    this.initializeBoard();
  }

  ngOnInit() {
    this.initializeBoard();
  }

  initializeBoard(){
    this.prepareBoardTableData();
    this.setShootingEnabled();
    this.initializeFields();
  }

  private initializeFields(){
    this.shotsSet = new Set<string>();
    this.shotsCount = 0;
  }

  private setShootingEnabled(){
    this.shootingEnabled = !this.gameFinished && this.playerTurn == this.playerId;
  }

  private prepareBoardTableData() {
    let ind = 0;
    for (let row of this.boardPlayer.board) {
      let columns: string[];
      columns = row.split("");
      let map = {};
      for (let i = 0; i < 10; i++) {
        map[String.fromCharCode('A'.charCodeAt(0) + i)] = columns[i];
      }
      this.board[ind] = map;
      ind++;
    }
    this.dataSource = new MatTableDataSource(this.board);
  }

  public getCellIcon(cellString: string){
    if (cellString == '#' && this.opponent) return ".";
    else return cellString;
  }

  public fireShot(index: number, column: string){
    let shot = index+1 + "x" + column;
    if (!this.shotsSet.has(shot)){
      this.shotsCount++;
      this.shotsSet.add(shot);
      if (this.shotsCount == 5){
        this.pushSalvoShots();
      }
    }
  }

  private pushSalvoShots() {
    let shots = new SalvoShots();
    shots.salvo = Array.from( this.shotsSet);

    this.gameService.fireShots(this.playerId, this.gameId, shots).subscribe(
        (response: ShotsResponse) => {
          let shots = response.salvo;
          let string ="";
          Object.keys(shots).forEach(key => {
            string = string + "\n" + key + " " + shots[key];
          });
          this.handleSuccess();
          alert(string);
        },
        err => this.handleError('Error occurred while retrieving all players data.', err));
  }

  public handleError(msg: string, error: HttpErrorResponse) {
    let message = 'Error occurred!';
    if (error.status == NOT_FOUND ) {
      message = msg + " not found.";
    }
    this.snackBar.open(message, 'ERROR', { duration: 2000, verticalPosition: 'top'});
  }

  public handleSuccess(){
    this.snackBar.open('Successfully fired shots.', 'SUCCESS', { duration: 2000, verticalPosition: 'top', panelClass: 'snackBar-success' });
  }

  public hitAlready(index: number, column: string): boolean{
    let shot = index+1 + "x" + column;
    return this.shotsSet != undefined && this.shotsSet.has(shot);
  }

}
