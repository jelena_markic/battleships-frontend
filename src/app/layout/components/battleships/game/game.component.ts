import {Component, ContentChild, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar} from "@angular/material";
import {PusherService} from "../../../../shared/services/pusher.service";
import {GameService} from "../../../../shared/services/game.service";
import {ActivatedRoute, Router} from '@angular/router';
import {PlayerService} from '../../../../shared/services/player.service';
import {GameStatus, GameStatusResponse} from '../../../../shared/model/game-status-response';
import {HttpErrorResponse} from '@angular/common/http';
import {NOT_FOUND} from 'http-status-codes';
import {Player} from '../../../../shared/model/player';
import {forkJoin, Observable, Subject} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {GameStatusEnum} from '../../../../shared/model/game-status-enum';
import {BoardComponent} from './board/board.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
  providers: [PusherService, GameService, PlayerService]
})
export class GameComponent implements OnInit {

  playerService: PlayerService;
  gameService: GameService;

  playerLoaded: boolean;

  gameStatusResponse: GameStatusResponse;
  player: Player;
  opponentPlayer: Player;

  boardUpdated: string;

  gameStatus: string;
  playerTurn: string;
  gameId: number;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private playerService1: PlayerService,
               private gameService1: GameService,
               private pusherService: PusherService,
               private snackBar: MatSnackBar) {
    this.playerService = playerService1;
    this.gameService = gameService1
  }

  ngOnInit() {
    this.playerLoaded = false;

    let gameId = this.route.snapshot.paramMap.get('game_id');
    let player_id = this.route.snapshot.paramMap.get('player_id');
    if (gameId != null && player_id != null){
      this.gameId = Number(gameId);
      this.loadGameData(gameId, player_id);
    }
    this.subScribeToLogs(this.gameId);
  }

  private setGameStatus(){
    if (this.gameStatusResponse.game.won != null){
      if (this.gameStatusResponse.game.won == this.player.id)
        this.gameStatus = GameStatusEnum.WON;
      else
        this.gameStatus = GameStatusEnum.LOST;
    } else
      this.gameStatus = "IN PROGRESS";
  }

  private setPlayerTurn(){
    if (this.gameStatusResponse.game.won == null){
      if (this.gameStatusResponse.game.player_turn == this.player.id)
        this.playerTurn = this.player.name + " (" + this.player.id + ")";
      else
        this.playerTurn = this.opponentPlayer.name + " (" + this.opponentPlayer.id + ")";
    }
  }

  public requestProfilesFromMultipleSources(playerId, opponentPlayerId): Observable<any[]> {
    let response1 = this.loadPlayerData(playerId);
    let response2 = this.loadPlayerData(opponentPlayerId);

    return forkJoin([response1, response2]);
  }

  public loadPlayerData(playerId){
    return this.playerService.playerProfile(playerId);
  }

  public loadGameData(gameId: string, playerId: string){

    this.gameService.loadGameData(gameId, playerId).subscribe(
        (response: GameStatusResponse) => {
          this.gameStatusResponse = response;
          this.requestProfilesFromMultipleSources(playerId, this.gameStatusResponse.opponent.player_id).subscribe(responseList => {
            this.player = responseList[0];
            this.opponentPlayer = responseList[1];
            this.setGameStatus();
            this.setPlayerTurn();
            this.playerLoaded = true;
          });

        },
        err => this.handleError('Game.', err));
  }


  subScribeToLogs(gameId: number) {
    this.pusherService.channel.bind(environment.pusher.event + this.gameId, data => {
      this.boardUpdated = "Board updated at: "  + new Date();
      this.loadGameData(gameId.toString(), this.player.id.toString());

    });
  }

public handleError(msg: string, error: HttpErrorResponse) {
  let message = 'Error occurred!';
  if (error.status == NOT_FOUND ) {
    message = msg + " not found.";
  }
  this.snackBar.open(message, 'ERROR', { duration: 2000, verticalPosition: 'top'});
}

}
