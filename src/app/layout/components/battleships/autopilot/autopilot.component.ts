import {Component, OnInit, ViewChild} from '@angular/core';
import {GameService} from '../../../../shared/services/game.service';
import {MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {GameDetail, GameFullList} from '../../../../shared/model/game-full-list';

@Component({
  selector: 'app-autopilot',
  templateUrl: './autopilot.component.html',
  styleUrls: ['./autopilot.component.scss'],
  providers: [GameService]
})
export class AutopilotComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('challengeForm') challengeForm;

  gameService: GameService;

  displayedColumns: string[] =
      [ 'gameId', 'challengingPlayerName', 'challengingPlayerAutopilot', 'opponentPlayerName', 'opponentPlayerAutopilot', 'gameStatus'];
  dataSource = null;

  games: GameDetail[];

  dataLoaded = false;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private gameService1: GameService,
               private snackBar: MatSnackBar) {
    this.gameService = gameService1
  }

  ngOnInit() {
    this.loadAllGames();
  }

  public loadAllGames(){
    this.gameService.getAllGames().subscribe(
        (response: GameFullList) => {
          this.games = response.games;
          this.dataSource = new MatTableDataSource(this.games);
          setTimeout(() => {
            this.dataSource.sort = this.sort;
          });
          this.dataLoaded = true;
        },
        err => this.handleError('Error occurred while retrieving all games data.', err));
  }

  runAutoPilot(playerId: number, gameId: number, playerName: string) {
    console.log("Ran autopilot for " + playerId + " , " + gameId);
    this.gameService.startAutopilot(playerId, gameId).subscribe(
        (response: any) => {},
        err => this.handleError("Error occurred challenging a player.", err),
        () => {
          this.handleSuccess("Successfully started autopilot for player " + playerName + ", game:  " + gameId + ".");
          this.dataLoaded = false;
          this.loadAllGames();
        });
  }

  public handleSuccess(msg: string){
    this.snackBar.open(msg, 'SUCCESS', { duration: 2000, verticalPosition: 'top', panelClass: 'snackBar-success' });
  }


  public handleError(msg: string, error: HttpErrorResponse) {
    this.snackBar.open(msg, 'ERROR', { duration: 2000, verticalPosition: 'top'});
  }


}
