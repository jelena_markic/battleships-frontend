import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayersComponent } from './players.component';

describe('PlayersComponent', () => {
  let component: PlayersComponent;
  let fixture: ComponentFixture<PlayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  // beforeEach(async(() => { TestBed.configureTestingModule({
  //   declarations: [ LinkingToolComponent, GeneratedComponent ],
  //   imports: [MaterialModule, NoopAnimationsModule] }) .compileComponents(); }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
