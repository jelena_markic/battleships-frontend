import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {PlayerService} from '../../../../shared/services/player.service';
import {MatTableDataSource, MatSnackBar, MatSort, MatPaginator} from '@angular/material';
import {Player} from '../../../../shared/model/player';
import {PlayerList} from '../../../../shared/model/player-list';


@Component({
  selector: 'app-players-table',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss'],
  providers: [PlayerService]
})
export class PlayersComponent implements OnInit {

  constructor(public playerService1: PlayerService,
              private snackBar: MatSnackBar) {
    this.playerService = playerService1;
  }

  @ViewChild(MatSort) sort: MatSort;
  playerService: PlayerService;

  dataLoaded: boolean;
  displayedColumns: string[] = [ 'id', 'name', 'email', 'play'];
  dataSource = null;
  players: Player[];

  ngOnInit() {
    this.getData();
  }

  private getData() {
    this.playerService.getPlayers().subscribe(
        (response: PlayerList) => this.handleElemRetrieval(response),
        err => this.handleError('Error occurred while retrieving data.', err));
  }

  handleElemRetrieval(response: PlayerList) {
    if (response != null) {
      this.dataLoaded = true;
      this.dataSource = new MatTableDataSource(response.players);
      setTimeout(() => {
        this.dataSource.sort = this.sort;
      });
    }
  }

  public handleError(msg: string, error: any) {
      let message = 'Error occurred!';
      if (msg != null) {
        message = msg;
      }
      this.snackBar.open(message, 'ERROR', { duration: 2000, verticalPosition: 'top'});
    }

}
