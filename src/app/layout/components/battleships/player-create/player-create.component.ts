import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar} from "@angular/material";
import {PlayerService} from "../../../../shared/services/player.service";
import {Player} from '../../../../shared/model/player';
import {HttpErrorResponse} from '@angular/common/http';
import {CONFLICT} from 'http-status-codes';
declare function require(path: string);

export interface DialogData {
  element: any;
}

@Component({
  selector: 'app-player-create',
  templateUrl: './player-create.component.html',
  styleUrls: ['./player-create.component.scss'],
  providers: [PlayerService]
})
export class PlayerCreateComponent implements OnInit {

  @ViewChild('playerForm') playerForm;

  title: string;
  saveButtonTitle: string;
  player: Player;

  playerService: PlayerService;

  constructor(
    private snackBar: MatSnackBar,
    public playerService1: PlayerService) {
      this.playerService = playerService1;
  }

  ngOnInit(): void {
    this.title = 'Add player';
    this.saveButtonTitle = 'Create';
    this.player = new Player();
  }

  onSubmit() {
    this.playerService.registerPlayer(this.player).subscribe(
        (response: Player) => {},
       err => this.handleError(err),
        () => this.handleSuccess());
  }

  public handleSuccess(){
    this.snackBar.open('Successfully saved.', 'SUCCESS', { duration: 2000, verticalPosition: 'top', panelClass: 'snackBar-success' });
    this.player = new Player();
    this.playerForm.resetForm();
  }

  public handleError(err: HttpErrorResponse){
    let message = 'Error occurred!';
    if (err.status == CONFLICT)
      message = "Player with the same email already exists.";

    this.snackBar.open(message, 'ERROR', { duration: 2000, verticalPosition: 'top', panelClass: 'snackBar-fail'});
  }

}
