import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import {PlayersComponent} from './components/battleships/players/players.component';
import {PlayerCreateComponent} from './components/battleships/player-create/player-create.component';
import {GamesComponent} from './components/battleships/games/games.component';
import {GameComponent} from './components/battleships/game/game.component';
import {AutopilotComponent} from './components/battleships/autopilot/autopilot.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
          {
            path: '',
            redirectTo: 'players'
          },
          {
              path: 'players',
              component: PlayersComponent
          },
          {
              path: 'register',
              component: PlayerCreateComponent
          },
          {
              path: 'games',
              component: GamesComponent
          },
          {
              path: 'autopilot',
              component: AutopilotComponent
          },
          {
              path: 'games/:player_id',
              component: GamesComponent
          },
          {
              path: 'games/:player_id/:game_id',
              component: GameComponent
          }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
