// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // restApiEndpoint: 'http://ec2-3-122-232-32.eu-central-1.compute.amazonaws.com:8080',
  restApiEndpoint: 'http://localhost:8080',
  pusher: {
    key: 'cc4b359b52283aa6b0d0',
    cluster: 'eu',
    channel: 'battleships',
    event: 'shots-',
    disableStats: true
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
